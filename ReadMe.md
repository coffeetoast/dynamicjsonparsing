서버에서 [Any]로 올때 CellType에 따른 Dynamic Json Parsing

- Swift4 Codable로 Parsing 하였습니다. 
- CellType에 따른 Enum으로 받아서 Parsing 하였습니다.
- enum CellType: String, Decodable {
    case company = "CELL_TYPE_COMPANY"
    case horizontal = "CELL_TYPE_HORIZONTAL_THEME"
    case interview = "CELL_TYPE_INTERVIEW"
    case jobPosting = "CELL_TYPE_JOB_POSTING"
    case review = "CELL_TYPE_REVIEW"
    case salary = "CELL_TYPE_SALARY"
}