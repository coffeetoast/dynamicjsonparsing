//
//  HomeData.swift
//  iOSTest
//
//  Created by SungJae Lee on 2018. 3. 27..
//  Copyright © 2018년 SungJae Lee. All rights reserved.
//

import Foundation

enum CellType: String, Decodable {
    case company = "CELL_TYPE_COMPANY"
    case horizontal = "CELL_TYPE_HORIZONTAL_THEME"
    case interview = "CELL_TYPE_INTERVIEW"
    case jobPosting = "CELL_TYPE_JOB_POSTING"
    case review = "CELL_TYPE_REVIEW"
    case salary = "CELL_TYPE_SALARY"
}

struct HomeData: Decodable {
    let items: [ItemType]
    
    enum ItemType: Decodable
    {
        case company(Company)
        case horizontal(Horizontal)
        case interview(Interview)
        case jobPosting(JobPosting)
        case review(Review)
        case salary(Salary)
        
        init(from decoder: Decoder) throws
        {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            let type = try container.decode(CellType.self, forKey: .cell_type)
            switch type
            {
            case .company: self = .company(try Company(from: decoder))
            case .horizontal: self = .horizontal(try Horizontal(from: decoder))
            case .interview: self = .interview(try Interview(from: decoder))
            case .jobPosting: self = .jobPosting(try JobPosting(from: decoder))
            case .review: self = .review(try Review(from: decoder))
            case .salary: self = .salary(try Salary(from: decoder))
            }
        }
        
        struct Company: Decodable {
            let logo_path: URL
            let name: String
            let industry_name: String
            let review_summary: String
            let salary_avg: Int
            let interview_question: String
            //let cell_type: String
        }
        
        struct Horizontal: Decodable {
            //let cell_type: String
            struct Theme: Decodable {
                let cover_image: URL
                let title: String
            }
            
            let themes: [Theme]
        }
        
        struct Interview: Decodable {
            //let cell_type: String
            let logo_path: URL
            let name: String
            let industry_name: String
            let interview_question: String
        }
        
        struct JobPosting: Decodable {
            //let cell_type: String
            struct Deadline: Decodable {
                let message: String
            }
            
            let logo: URL
            let company_name: String
            let title: String
            let deadline: Deadline
            
        }
        struct Review: Decodable {
            //let cell_type: String
            let logo_path: URL
            let name: String
            let industry_name: String
            let review_summary: String
            let pros: String
            let cons: String
        }
        struct Salary: Decodable {
            //let cell_type: String
            let logo_path: URL
            let name: String
            let industry_name: String
            let salary_avg: Int
            let salary_highest: Int
            let salary_lowest: Int
        }
        
        enum CodingKeys: String, CodingKey
        {
            case cell_type
        }
    }
    
    enum CodingKeys: String, CodingKey
    {
        case items
    }
}
