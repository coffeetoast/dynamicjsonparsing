//
//  ReviewTableViewCell.swift
//  iOSTest
//
//  Created by SungJae Lee on 2018. 3. 28..
//  Copyright © 2018년 SungJae Lee. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class ReviewTableViewCell: UITableViewCell {
    var item: HomeData.ItemType.Review? {
        didSet {
            if let item = item {
                Alamofire.request(item.logo_path).responseImage { response in
                    if let image = response.result.value {
                        print(image)
                        self.logoImg.image = image
                    }
                }

                self.name.text = item.name
                self.industry_name.text = item.industry_name
                self.review_summary.text = item.review_summary
                self.pros.text = item.pros
                self.cons.text = item.cons
            }
        }
    }
    
    
    @IBOutlet var logoImg: UIImageView!
    @IBOutlet var name: UILabel!
    @IBOutlet var industry_name: UILabel!
    @IBOutlet var review_summary: UILabel!
    @IBOutlet var pros: UILabel!
    @IBOutlet var cons: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
