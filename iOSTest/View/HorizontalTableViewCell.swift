//
//  HorizontalTableViewCell.swift
//  iOSTest
//
//  Created by SungJae Lee on 2018. 3. 28..
//  Copyright © 2018년 SungJae Lee. All rights reserved.
//

import UIKit

class HorizontalTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    var item: HomeData.ItemType.Horizontal?
    
    let cellId = "HcellId"
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var layout: UICollectionViewFlowLayout!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupView()
    }
    
    func setupView() {
        backgroundColor = .groupTableViewBackground
        collectionView.register(UINib(nibName: "HCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: cellId)
        //layout.estimatedItemSize = CGSize(width: 1, height: 1)
        collectionView.backgroundColor = .groupTableViewBackground
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return item?.themes.count ?? 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as? HCollectionViewCell else { fatalError("Unexpected CollectionView Cell")}
        
        cell.item = item?.themes[indexPath.row]
        return cell
    }
    
}

