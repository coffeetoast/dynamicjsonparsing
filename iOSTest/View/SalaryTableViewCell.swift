//
//  SalaryTableViewCell.swift
//  iOSTest
//
//  Created by SungJae Lee on 2018. 3. 27..
//  Copyright © 2018년 SungJae Lee. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class SalaryTableViewCell: UITableViewCell {
    var item: HomeData.ItemType.Salary? {
        didSet {
            if let item = item {
                Alamofire.request(item.logo_path).responseImage { response in
                    if let image = response.result.value {
                        print(image)
                        self.logoImg.image = image
                    }
                }
              
                self.name.text = item.name
                self.industry_name.text = item.industry_name
                self.salary_avg.text = String(item.salary_avg)
                self.salary_lowest.text = String(item.salary_lowest)
                self.salary_highest.text = String(item.salary_highest)
            }
        }
    }
    
    @IBOutlet var logoImg: UIImageView!
    @IBOutlet var name: UILabel!
    @IBOutlet var industry_name: UILabel!
    @IBOutlet var salary_avg: UILabel!
    @IBOutlet var salary_lowest: UILabel!
    @IBOutlet var salary_highest: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
