//
//  InterviewTableViewCell.swift
//  iOSTest
//
//  Created by SungJae Lee on 2018. 3. 27..
//  Copyright © 2018년 SungJae Lee. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class InterviewTableViewCell: UITableViewCell {
    var item: HomeData.ItemType.Interview? {
        didSet {
            if let item = item {
                Alamofire.request(item.logo_path).responseImage { response in
                    if let image = response.result.value {
                        print(image)
                        self.logoImg.image = image
                    }
                }
                
                self.name.text = item.name
                self.industry_name.text = item.industry_name
                self.interview_question.text = item.interview_question
            }
        }
    }
    
    @IBOutlet var logoImg: UIImageView!
    @IBOutlet var name: UILabel!
    @IBOutlet var industry_name: UILabel!
    @IBOutlet var interview_question: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
