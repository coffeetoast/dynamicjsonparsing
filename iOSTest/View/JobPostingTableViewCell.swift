//
//  JobPostingTableViewCell.swift
//  iOSTest
//
//  Created by SungJae Lee on 2018. 3. 27..
//  Copyright © 2018년 SungJae Lee. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class JobPostingTableViewCell: UITableViewCell {
    var item: HomeData.ItemType.JobPosting? {
        didSet {
            if let item = item {
                Alamofire.request(item.logo).responseImage { response in
                    if let image = response.result.value {
                        print(image)
                        self.logoImg.image = image
                    }
                }
                
                self.name.text = item.company_name
                self.industry_name.text = item.title
                self.message.text = item.deadline.message
            }
        }
    }
    

    @IBOutlet var logoImg: UIImageView!
    @IBOutlet var name: UILabel!
    @IBOutlet var industry_name: UILabel!
    @IBOutlet var message: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
