//
//  HCollectionViewCell.swift
//  iOSTest
//
//  Created by SungJae Lee on 2018. 3. 28..
//  Copyright © 2018년 SungJae Lee. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class HCollectionViewCell: UICollectionViewCell {
    var item: HomeData.ItemType.Horizontal.Theme? {
        didSet {
            if let item = item {
                Alamofire.request(item.cover_image).responseImage { response in
                    if let image = response.result.value {
                        print(image)
                        self.cover_image.image = image
                    }
                }
                
                self.title.text = item.title
            }
        }
    }
    
    @IBOutlet var cover_image: UIImageView!
    @IBOutlet var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
  

}
