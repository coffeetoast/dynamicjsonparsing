//
//  ViewController.swift
//  iOSTest
//
//  Created by SungJae Lee on 2018. 3. 27..
//  Copyright © 2018년 SungJae Lee. All rights reserved.
//

import UIKit
import Alamofire

class TableViewController: UITableViewController {
    var items = [HomeData.ItemType]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadJSON()
        registerCell()
        setupTableView()
    }
    
    func loadJSON() {
        guard let url = URL(string: Config.baseURLString) else { fatalError("Unable to find Json URL")}
        
        Alamofire.request(url).responseJSON { (response) in
            //print(Thread.isMainThread)
            if let data = response.data {
                do {
                    let homeData = try JSONDecoder().decode(HomeData.self, from: data)
                    self.items = homeData.items
                    self.tableView.reloadData()
                } catch {
                    print(error)
                }
            }
        }
    }
    
    private func registerCell() {
        tableView.register(UINib(nibName: "CompanyTableViewCell", bundle: nil)
            , forCellReuseIdentifier: CellType.company.rawValue)
        tableView.register(UINib(nibName: "HorizontalTableViewCell", bundle: nil), forCellReuseIdentifier: CellType.horizontal.rawValue)
        tableView.register(UINib(nibName: "InterviewTableViewCell", bundle: nil), forCellReuseIdentifier: CellType.interview.rawValue)
        tableView.register(UINib(nibName: "JobPostingTableViewCell", bundle: nil), forCellReuseIdentifier: CellType.jobPosting.rawValue)
        tableView.register(UINib(nibName: "ReviewTableViewCell", bundle: nil), forCellReuseIdentifier: CellType.review.rawValue)
        tableView.register(UINib(nibName: "SalaryTableViewCell", bundle: nil), forCellReuseIdentifier: CellType.salary.rawValue)
    
    }
    
    private func setupTableView() {
        self.title = "CodingTest"
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        tableView.separatorStyle = .none
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellType = self.items[indexPath.row]
        switch cellType {
            
        case .company(let item): return setupCompanyTableViewCell(indexPath,data: item)
        case .horizontal(let item): return setupHorizontalTableViewCell(indexPath,data: item)
        case .interview(let item): return setupInterviewTableViewCell(indexPath,data: item)
        case .jobPosting(let item): return setupJobPostingTableViewCell(indexPath,data: item)
        case .review(let item): return setupReviewTableViewCell(indexPath,data: item)
        case .salary(let item): return setupSalaryTableViewCell(indexPath,data: item)
            
        }
        
    }
    
    private func setupCompanyTableViewCell(_ indexPath: IndexPath, data: HomeData.ItemType.Company) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellType.company.rawValue, for: indexPath) as? CompanyTableViewCell else { fatalError("Unexpected Table View Cell") }
        
        cell.item = data
        
        return cell
    }
    
    private func setupHorizontalTableViewCell(_ indexPath: IndexPath, data: HomeData.ItemType.Horizontal) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellType.horizontal.rawValue, for: indexPath) as? HorizontalTableViewCell else { fatalError("Unexpected Table View Cell") }
        
        cell.item = data
        
        return cell
    }
    
    private func setupInterviewTableViewCell(_ indexPath: IndexPath,data: HomeData.ItemType.Interview) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellType.interview.rawValue, for: indexPath) as? InterviewTableViewCell else { fatalError("Unexpected Table View Cell") }
        
        cell.item = data
        
        return cell
    }
    
    private func setupJobPostingTableViewCell(_ indexPath: IndexPath, data: HomeData.ItemType.JobPosting) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellType.jobPosting.rawValue, for: indexPath) as? JobPostingTableViewCell else { fatalError("Unexpected Table View Cell") }
       
        cell.item = data
        
        return cell
    }
    
    private func setupReviewTableViewCell(_ indexPath: IndexPath, data: HomeData.ItemType.Review) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellType.review.rawValue, for: indexPath) as? ReviewTableViewCell else { fatalError("Unexpected Table View Cell") }
        
        cell.item = data

        return cell
    }
    
    private func setupSalaryTableViewCell(_ indexPath: IndexPath, data: HomeData.ItemType.Salary) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellType.salary.rawValue, for: indexPath) as? SalaryTableViewCell else { fatalError("Unexpected Table View Cell") }
        
        cell.item = data
        return cell
    }
    
}

