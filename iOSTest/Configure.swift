//
//  Configure.swift
//  iOSTest
//
//  Created by SungJae Lee on 2018. 3. 27..
//  Copyright © 2018년 SungJae Lee. All rights reserved.
//

import Foundation

struct Config {
    static let baseURLString = "https://api-test-198703.appspot.com"
}
